#! /bin/bash

cd scripts

./requirement.sh
./db.sh
./ver.sh
./thingspeak.sh
./afterthingspeak.sh
./autorunpi3.sh
./lib.sh

cd ~/thingspeak/
rails server

