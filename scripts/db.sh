#!/bin/bash
echo "In mysql, configure and flush it."

sudo apt-get install mysql-server

#sudo mysql -u root -e "USE mysql; UPDATE user SET password=PASSWORD('root') WHERE User='root' AND 'localhost'; FLUSH PRIVILEGES; \q;"

#sudo service mysql restart

sudo /etc/init.d/mysql stop

mysqld_safe --skip-grant-tables

sudo systemctl start mysqld.service

sudo mysql -u root -e "grant all privileges on *.* to 'root'@'localhost' identified by 'root' with grant option; \q;"
