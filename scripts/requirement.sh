#!/bin/bash
echo -e "\033[33m ************************ \033[0m"
echo -e "\033[33m Installing support environment \033[0m"
echo -e "\033[33m uninstalling present ruby and install rvm \033[0m"
echo -e "\033[33m ************************ \033[0m"
sudo apt-get purge ruby -y
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install build-essential 
sudo apt-get -y install git
sudo apt-get -y install libxml2-dev
sudo apt-get -y install libxslt-dev 
sudo apt-get -y install libssl-dev
sudo apt-get -y install libsqlite3-dev
sudo apt-get -y install autoconf
sudo apt-get -y install bison
sudo apt-get -y install libyaml-dev
sudo apt-get -y install libreadline6-dev
sudo apt-get -y install zlib1g-dev
sudo apt-get -y install libncurses5-dev
sudo apt-get -y install libffi-dev
sudo apt-get -y install libgdbm3
sudo apt-get -y install libgdbm-dev

echo -e "\033[33m ************************ \033[0m"
echo -e "\033[33m Installing Ruby Version Manager (rvm) \033[0m"
echo -e "\033[33m use ruby 2.1.9 \033[0m"
echo -e "\033[33m ************************ \033[0m"

#curl -sSL https://rvm.io/mpapis.asc | sudo gpg --import -

curl -L https://get.rvm.io | sudo bash -s stable --ruby=2.1.9

rvm use 2.1.9
