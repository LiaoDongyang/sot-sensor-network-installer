#!/bin/bash

cd ~
git clone https://github.com/iobridge/thingspeak.git

cd thingspeak/

bundle install

cp thingspeak/config/database.yml.example thingspeak/config/database.yml
