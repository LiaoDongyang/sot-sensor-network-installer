#! /bin/bash

sudo apt-get install apache2 -y

sudo  cp -rf ./lib/* /var/www/html/

cd /var/www/html/

sudo chmod 0777 PicProc
sudo chmod 0777 DataAnalysisProc

cd PicProc
sudo mkdir Cache
sudo chmod 0777 Cache con sig
sudo cp picproc.service /etc/systemd/system/
systemctl start picproc.service
cd ..

cd DataAnalysisProc
sudo chmod 0777 DataQueue
sudo cp DAP.service /etc/systemd/system/
systemctl start DAP.service
cd ..

