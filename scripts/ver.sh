#!/bin/bash

echo -e "\033[33m ************************ \033[0m"
echo -e "\033[33m ruby gem install speical software version \033[0m"
echo -e "\033[33m ************************ \033[0m"
source /etc/profile.d/rvm.sh
rvm use 2.1.9

#
#  sudo visudo 
#  make default path #
#

echo "gem: --no-rdoc --no-ri" >> ${HOME}/.gemrc
sudo gem install activesupport --no-rdoc --no-ri -v 4.2.6
sudo gem install nokogiri -v '1.6.3.1' -- --use-system-libraries
sudo gem install rails --no-rdoc --no-ri -v 4.2.6
sudo gem install bundle --no-rdoc --no-ri
gem uninstall libv8
gem install therubyracer -v '0.12.1'
gem install libv8 -v '3.16.14.7' -- --with-system-v8
gem install tzinfo -v "1.1.0" # use 1.1.0
gem uninstall tzinfo high level
gem install tzinfo-data -v "1.2013.8" # use


sudo apt-get install libmariadbclient-dev-compat

