#! /bin/bash

echo "set ruby ver 2.1.9"
[[ -s "/usr/local/rvm/scripts/rvm" ]] && source "/usr/local/rvm/scripts/rvm"  # This loads RVM into a shell session.
source ~/.rvm/scripts/rvm
rvm use 2.1.9
ruby --version || echo

echo "run the thingspeak"
cd ~/thingspeak || /dev/null 2>&1
echo "use 2.1.9"
rails server webrick
